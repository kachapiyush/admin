var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var session = require("express-session");
var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");

var app = express();

var pool = require("./config/db.js");

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(
  session({
    secret: "app",
    cookie: { maxAge: 1800000 },
    resave: true,
    saveUninitialized: true,
  })
);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

/*Authentication Check Start*/
app.get("/login", function (req, res, next) {
  res.render("login", { message: "" });
});
app.post("/login", function (req, res, next) {
  var email = req.body.email;
  var password = req.body.password;
  pool.getConnection((err, connection) => {
    if (err) throw err;
    connection.query(
      `SELECT * from users where email='${email}' and password='${password}'`,
      (err, rows) => {
        connection.release(); // return the connection to pool
        if (err) throw err;
        if (rows.length == 1) {
          console.log("user found");
          req.session.loggedIn = true;
          console.log(req.session.loggedIn);
          res.render("index");
        } else {
          res.render("login", { message: "Email and password not match" });
        }
      }
    );
  });
});
app.get("/logout", function (req, res, next) {
  req.session.loggedIn = false;
  res.redirect("/");
});

/*var checkUser = function (req, res, next) {
  if (req.session.loggedIn) {
    next();
  } else {
    res.render("login", { message: "Login First Dear" });
  }
};*/

app.use(function (req, res, next) {
  console.log(req.session.loggedIn);
  if (req.session.loggedIn) {
    next();
  } else {
    res.render("login", { message: "Login First Dear" });
  }
});

/* Authentication Route End*/
app.use("/", indexRouter);
app.use("/users", usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
