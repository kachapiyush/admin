var express = require("express");
var mysql = require("mysql");
var router = express.Router();
var pool = require("../config/db.js");

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("login", { message: "" });
});
router.get("/login", function (req, res, next) {
  res.render("login", { message: "" });
});

/*Login Page*/

/*Register Page*/
router.get("/register", function (req, res, next) {
  res.render("register");
});

/*Password Page*/
router.get("/forgotpassword", function (req, res, next) {
  res.render("forgotpassword");
});

/*Stock Page*/
router.get("/stocks", function (req, res, next) {
  res.render("stocks");
});

/*Users Page*/
router.get("/users", function (req, res, next) {
  var results;
  pool.getConnection((err, connection) => {
    if (err) throw err;
    connection.query("SELECT * from users ORDER BY userid", (err, rows) => {
      connection.release(); // return the connection to pool
      if (err) throw err;
      res.render("users", { userData: rows });
    });
  });
});

router.post("/adduser", function (req, res, next) {
  var firstname = req.body.firstname;
  var lastname = req.body.lastname;
  var email = req.body.email;
  var password = req.body.password;

  var query = `INSERT INTO users (firstname, lastname, email, password)VALUES ('${firstname}', '${lastname}', '${email}', '${password}')`;
  pool.getConnection((err, connection) => {
    if (err) throw err;
    connection.query(query, (err, rows) => {
      connection.release();
      if (err) throw err;
      //res.send('<script>alert("USer Created")</script>');
      res.redirect("/users");
    });
  });
});

router.get("/edituser/:id", function (req, res, next) {
  var id = req.params.id;
  var query = `select * from users where userid = ${id}`;
  pool.getConnection((err, connection) => {
    if (err) throw err;
    connection.query(query, (err, rows) => {
      connection.release();
      if (err) throw err;
      //console.log(rows);

      res.render("editusermodal", { userData: rows });
    });
  });
});

router.post("/usersave", function (req, res, next) {
  var userid = req.body.userid;
  var firstname = req.body.firstname;
  var lastname = req.body.lastname;
  var email = req.body.email;
  var password = req.body.password;
  var query = `UPDATE users
SET firstname = '${firstname}', lastname= '${lastname}',email='${email}',password='${password}'
WHERE userid = ${userid}`;
  pool.getConnection((err, connection) => {
    if (err) throw err;
    connection.query(query, (err, rows) => {
      connection.release();
      if (err) throw err;
      //console.log(rows);
      res.redirect("/users");
    });
  });
});

router.post("/userdelete", function (req, res, next) {
  var userid = req.body.userid;
  //console.log(userid);
  var query = `DELETE FROM users WHERE userid = ${userid}`;
  pool.getConnection((err, connection) => {
    if (err) throw err;
    connection.query(query, (err, rows) => {
      connection.release();
      if (err) throw err;
      console.log(rows);
      res.redirect("/users");
    });
  });
});
module.exports = router;
